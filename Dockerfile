FROM openjdk:19-alpine
COPY build/libs/redisexample-0.0.1-SNAPSHOT.jar /redisexample-app/
CMD ["java", "-jar", "/redisexample-app/redisexample-0.0.1-SNAPSHOT.jar"]