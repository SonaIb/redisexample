package com.lesson.redisexample.repository;

import com.lesson.redisexample.model.Account;
import jakarta.persistence.LockModeType;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {
    @Override
   // @Cacheable(key = "#id", cacheNames = "account")
    @Lock(LockModeType.PESSIMISTIC_READ)
    Optional<Account> findById(Long id);

}
