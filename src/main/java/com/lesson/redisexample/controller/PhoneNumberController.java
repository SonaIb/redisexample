package com.lesson.redisexample.controller;

import com.lesson.redisexample.model.PhoneNumber;
import com.lesson.redisexample.service.PhoneNumberServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/phone")
@RequiredArgsConstructor
public class PhoneNumberController {

    private final PhoneNumberServiceImpl phoneServiceImpl;

    @GetMapping("/{id}")
    public PhoneNumber getPhoneNumber(@PathVariable Long id) {
        return phoneServiceImpl.getById(id);
    }

}
