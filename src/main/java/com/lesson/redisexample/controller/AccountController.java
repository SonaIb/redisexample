package com.lesson.redisexample.controller;

import com.lesson.redisexample.model.Account;
import com.lesson.redisexample.service.AccountServiceImpl;
import com.lesson.redisexample.service.AccountServiceImpl2;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/account")
@RequiredArgsConstructor
public class AccountController {
    private final AccountServiceImpl accountServiceImpl;
    private final AccountServiceImpl2 accountServiceImpl2;

    @GetMapping("/all")
    public List<Account> findAll() {
        return accountServiceImpl.findAll();
    }


    @GetMapping("/{id}")
    public Account getAccount(@PathVariable Long id) {
        Account byId = accountServiceImpl2.getById(id);
        return byId;
    }

     @PutMapping
    public Account update(@RequestBody Account account){
        return accountServiceImpl.update(account);
     }

     @DeleteMapping("/{id}")
    public Account delete(@PathVariable Long id){
        return accountServiceImpl.delete(id);
     }

}
