package com.lesson.redisexample.controller;

import com.lesson.redisexample.model.Account;
import com.lesson.redisexample.service.TransferService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/transfer")
@RequiredArgsConstructor
public class TransferController {

private final TransferService transferService;

@GetMapping("/update")
public void transfer(){
    transferService.update();
}
    @GetMapping("/update2")
    public void transfer2(){
        transferService.update2();
    }

@GetMapping("/read")
public Account read(){
   return  transferService.read();
}
}
