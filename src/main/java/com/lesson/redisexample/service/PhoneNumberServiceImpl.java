package com.lesson.redisexample.service;

import com.lesson.redisexample.model.PhoneNumber;
import com.lesson.redisexample.repository.PhoneNumberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PhoneNumberServiceImpl implements PhoneNumberService {

    private final PhoneNumberRepository phoneNumberRepository;
    @Override
    public List<PhoneNumber> findAll() {
        return null;
    }


    @Override
    @Cacheable(key="#id", cacheNames = "phoneNumber")
    public PhoneNumber getById(Long id) {
        return phoneNumberRepository.findById(id).get();
    }

    @Override
    public PhoneNumber update(PhoneNumber phoneNumber) {
        return null;
    }

    @Override
    public PhoneNumber delete(Long id) {
        return null;
    }
}
