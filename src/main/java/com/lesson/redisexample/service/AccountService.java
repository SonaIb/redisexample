package com.lesson.redisexample.service;

import com.lesson.redisexample.model.Account;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface AccountService {

    List<Account> findAll();

    Account getById(Long id);

    @Transactional
    Account update(Account account);

    Account delete(Long id);

}
