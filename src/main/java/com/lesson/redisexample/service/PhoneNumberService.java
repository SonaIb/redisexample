package com.lesson.redisexample.service;

import com.lesson.redisexample.model.PhoneNumber;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface PhoneNumberService {

    List<PhoneNumber> findAll();

    PhoneNumber getById(Long id);

    @Transactional
    PhoneNumber update(PhoneNumber phoneNumber);

    PhoneNumber delete(Long id);
}
