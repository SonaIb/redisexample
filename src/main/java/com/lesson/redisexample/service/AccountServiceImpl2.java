package com.lesson.redisexample.service;

import com.lesson.redisexample.model.Account;
import com.lesson.redisexample.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.util.List;

@RequiredArgsConstructor
@Service
@Primary
public class AccountServiceImpl2 implements AccountService {
    private final AccountRepository accountRepository;
    private final RedisTemplate<String, Object> redisTemplate;
    @Override
    public List<Account> findAll() {
        return accountRepository.findAll();
    }

    @Override
    public Account getById(Long id) {
        var account = (Account)redisTemplate.opsForValue().get(String.valueOf(id));
        if(account != null){
            return account;
        }
        Account account1 = accountRepository.findById(id).get();
        redisTemplate.opsForValue().set(String.valueOf(id), account1, Duration.ofSeconds(120));
        return account1;
    }

    @Override
    @Transactional
    public Account update(Account account) {
        Account account1 = accountRepository.findById(account.getId()).orElseThrow();
        account1.setName(account.getName());
        account1.setBalance(account.getBalance());
        return account1;
    }

    @Override
    public Account delete(Long id) {
        Account account = accountRepository.findById(id).orElseThrow();
        accountRepository.delete(account);
        return account;
    }
}
