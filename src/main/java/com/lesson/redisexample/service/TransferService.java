package com.lesson.redisexample.service;

import com.lesson.redisexample.model.Account;
import com.lesson.redisexample.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@RequiredArgsConstructor
public class TransferService {

    private final AccountRepository accountRepository;

    public void transfer(){
        log.info("Starting transfer on thread {}", Thread.currentThread().getId());
        int amount = 30;
        Account from = accountRepository.findById(2L).get();
        log.info("Getting account on thread{} with id 2L - {}", Thread.currentThread().getId(), from);
        if(from.getBalance() < 30){
            throw new RuntimeException("Not enough balance");
        }

        Account to = accountRepository.findById(3l).get();
        from.setBalance(from.getBalance() - amount);
        to.setBalance(to.getBalance() + 30);
        accountRepository.save(from);
        accountRepository.save(to);
    }

    @SneakyThrows
    @Transactional
    public Account update() {
        log.info("trying to get account...");
        Account account = accountRepository.findById(2L).get();//340
        log.info("trying to update balance...");
        account.setBalance(account.getBalance() + 50);//380
        Thread.sleep(10000);
        accountRepository.save(account);
        log.info("account saved success {}", account);
        return account;
    }

    @Transactional
    public Account update2() {
        log.info("trying to get account... #2");
        Account account = accountRepository.findById(2L).get();
        log.info("trying to update balance... #2");
        account.setBalance(account.getBalance() + 40);
        accountRepository.save(account);
        log.info("account saved success {} #2", account);
        return account;
    }

    @Transactional
    public Account read() {
        log.info("trying to read account...");
        Account account = accountRepository.findById(2l).get();
        log.info("Account read success {}", account);
        return account;
    }
}
