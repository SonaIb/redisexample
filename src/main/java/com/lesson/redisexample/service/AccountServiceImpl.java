package com.lesson.redisexample.service;

import com.lesson.redisexample.model.Account;
import com.lesson.redisexample.repository.AccountRepository;
import com.lesson.redisexample.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.cache.Cache;
import java.util.List;
@RequiredArgsConstructor
@Service
public class AccountServiceImpl implements AccountService {
    private final AccountRepository accountRepository;
    private final CacheManager cacheManager;
    @Override
    public List<Account> findAll() {
        return accountRepository.findAll();
    }

    @Override
    public Account getById(Long id) {
        Cache cache = cacheManager.getCache("account");
        Account accountFromCache = cache.get(id, Account.class);
       if (accountFromCache == null) {
          Account account = accountRepository.findById(id).get();
          cache.put(id, account);
            return account;
        }
       return accountFromCache;

    }

    @Override
    @Transactional
    public Account update(Account account) {
        Account account1 = accountRepository.findById(account.getId()).orElseThrow();
        account1.setName(account.getName());
        account1.setBalance(account.getBalance());
        return account1;
    }

    @Override
    public Account delete(Long id) {
        Account account = accountRepository.findById(id).orElseThrow();
        accountRepository.delete(account);
        return account;
    }
}
   /* @Override
    @Cacheable(key = "#root.methodName", cacheNames = "accounts")
    public List<Account> findAll() {
        return accountRepository.findAll();
    }

    @Override
  //  @Cacheable(key = "#id", cacheNames = "account")
    public Account getById(Long id) {
        return accountRepository.findById(id).get();
    }

    @Override
    @Transactional
    @CachePut(cacheNames = "account" , key="#account.id")
    public Account update(Account account) {
        Account account1 = accountRepository.findById(account.getId()).orElseThrow();
        account1.setName(account.getName());
        account1.setBalance(account.getBalance());
        return account1;
    }

    @Override
    @CacheEvict(cacheNames = "account", key="#id")
    public Account delete(Long id) {
        Account account = accountRepository.findById(id).orElseThrow();
        accountRepository.delete(account);
        return account;
    }*/
