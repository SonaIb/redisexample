package com.lesson.redisexample.model;


import jakarta.persistence.*;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;
import lombok.experimental.FieldNameConstants;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "account")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
@FieldNameConstants
@ToString
public class Account implements Serializable {
    public static final long serialVersionUID = 12331232134L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String name;
    Double balance;
    @OneToMany(mappedBy = "account", fetch = FetchType.EAGER)
    Set<PhoneNumber> phoneNumberList = new HashSet<>();

}
